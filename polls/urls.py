from django.urls import path
from . import views as polls_views


app_name = 'polls'


urlpatterns = [
    path('', polls_views.index, name = "index"),
    #ex: /polls/5/
    path('<int:question_id>/', polls_views.detail, name = "detail"),
    #ex /polls/5/results
    path('<int:question_id>/results', polls_views.result, name = "results"),
    #path /polls/5/vote
    path('<int:question_id>/vote', polls_views.vote, name = "vote"),

]


