from django.http import HttpResponse, Http404
from django.shortcuts import render, get_object_or_404
from django.template import loader
from .models import Question

def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]   #gli ultimi 5
    #output = ','.join([q.question_text for q in latest_question_list])
    template = loader.get_template('polls/index.html')
    context = {
        'latest_question_list': latest_question_list,
    }
    #return HttpResponse("Hi there, It's working")
    #return HttpResponse(template.render(context, request))
    #render(Arg[0] = request, arg[1] = template name, arg[2] = context)
    return render(request, 'polls/index.html', context) 

def detail(request, question_id):
    # try:
    #     question = Question.objects.get(pk = question_id)
    # except Question.DoesNotExist:
    #     raise Http404("Question doesn't exist")

    question = get_object_or_404(Question, pk = question_id)
    
    return render(request, 'polls/detail.html', {'question': question})


def result(request, question_id):
    response = "You are looking at the result of question %s."
    return HttpResponse(response % question_id)

def vote(request, question_id):
    return HttpResponse("You are voting on question %s." % question_id)

